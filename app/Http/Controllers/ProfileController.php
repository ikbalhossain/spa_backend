<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $data = [
            "full_name"               => "Md Ikbal Hossain",
            "father_name"             => "Md Moinal Hossain",
            "profile_image"           => "ikbal.jpg",
            "profile_image_directory" => "directory_one",
            "birth_date"              => "12-05-1989",
            "current_age"             => "28 years old",
            "email_address"           => "md.ikbalhossain@gmail.com",            
            "experience"              => "5 years",
            "mobile_number"           => "+88017262109373",
            "fax_number"              => "",
            "website"                 => "",
            "present_address"         => "",
            "permanent_address"       => "",
            "job_type"       => "MID Level",
            "last_degree"       => "Masters In Computer Application",
            "social_media"  => [
                "facebook"    => "",
                "twitter"     => "",
                "linkdin"     => "",
                "google_plus" => "",                
                "skype"       => "",                
            ],
            "education"     =>[
                [
                    "institution" =>"BRAC University",
                    "degree_name" =>"Master In Computer Application",
                    "duration"    =>"2 Years"
                ],
                [
                    "institution" =>"Stamford University",
                    "degree_name" =>"BSC in Electrical & Electronics",
                    "duration"    =>"4 Years"
                ],
            ],
            "job_history"=>[]

        ];
        $profile_array = [
            "status"=>"ok",
            "data"=>$data
        ];
        return json_encode($profile_array);
    }
}
